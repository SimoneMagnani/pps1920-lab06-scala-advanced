package u06lab.code

import scala.collection.mutable

object TicTacToe extends App {
  sealed trait Player{
    def other: Player = this match {case X => O; case _ => X}
    override def toString: String = this match {case X => "X"; case _ => "O"}
  }
  case object X extends Player
  case object O extends Player

  case class Mark(x: Int, y: Int, player: Player)
  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Int, y: Int): Option[Player] = board.find(m => m.x == x && m.y == y).map(m => m.player)

  def placeAnyMark(board: Board, player: Player): Seq[Board] =
    for (x <- 0 to 2;
         y <- 0 to 2
         if find(board, x, y).isEmpty)
      yield Mark(x, y, player) :: board

  val newGames:Game = Nil;

  def computeAnyGame(player: Player, moves: Int): Stream[Game] = templateComputeAnyGame(player, moves, _=>true)

  private def templateComputeAnyGame(player: Player, moves: Int, filter: Board => Boolean) : Stream[Game] = moves match {
    case n if n > 0 => for (game <- computeAnyGame(player.other, n - 1);
                            nextMove <- placeAnyMark(game.head, player)
                            if filter(nextMove))
      yield nextMove :: game
    case _ => Stream(List(List()))
  }

  def computeAnyGameWithStop(player: Player, moves: Int): Stream[Game] = templateComputeAnyGame(player, moves, IsBoardNotFinished)

  private def IsBoardNotFinished(board: Board): Boolean = {
    val gridSize = 3
    val list: mutable.MutableList [Int] = mutable.MutableList(0,0,0, 0,0,0, 0,0) //(row1,row2,row3, col1,col2,col3, diag1,diag2)
    for (mark <- board){
      val point:Int  = if (mark.player equals X) 1 else -1

      list(mark.x) += point                             // update rows
      list(gridSize + mark.y) += point                  // update cols

      if (mark.x == mark.y) list(2 * gridSize) += point // update diag1
      if (gridSize-1-mark.y == mark.x) list(2*gridSize + 1) += point //update diag2
    }
    !list.forall(num => (num == gridSize || num == -gridSize))
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ("."))
      if (x == 2) { print(" "); if (board == game.head) println()}
    }

  // Exercise 1: implement find such that..
  println(find(List(Mark(0,0,X)),0,0)) // Some(X)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),0,1)) // Some(O)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),1,1)) // None

  // Exercise 2: implement placeAnyMark such that..
  printBoards(placeAnyMark(List(),X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  printBoards(placeAnyMark(List(Mark(0,0,O)),X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  computeAnyGame(O, 4) foreach {g => printBoards(g); println()}
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!
}