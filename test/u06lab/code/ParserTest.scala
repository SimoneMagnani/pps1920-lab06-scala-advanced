package u06lab.code

import org.junit.Assert._
import org.junit.jupiter.api.Test

class ParserTest {

  @Test
  def basicParserTest() = {
    def parser = new BasicParser(Set('a', 'b', 'c'))

    assertTrue(parser.parseAll("aabc".toList)) // true
    assertFalse(parser.parseAll("aabcdc".toList)) // false
    assertTrue(parser.parseAll("".toList)) // true
  }

  @Test
  def parserNETest() = {
    // Note NonEmpty being "stacked" on to a concrete class
    // Bottom-up decorations: NonEmptyParser -> NonEmpty -> BasicParser -> Parser
    def parserNE = new NonEmptyParser(Set('0', '1'))

    assertTrue(parserNE.parseAll("0101".toList)) // true
    assertFalse(parserNE.parseAll("0123".toList)) // false
    assertFalse(parserNE.parseAll(List())) // false
  }

  @Test
  def parserNTCTest() = {
    def parserNTC = new NotTwoConsecutiveParser(Set('X', 'Y', 'Z'))

    assertTrue(parserNTC.parseAll("XYZ".toList)) // true
    assertFalse(parserNTC.parseAll("XYYZ".toList)) // false
    assertTrue(parserNTC.parseAll("".toList)) // true
  }

  @Test
  def parserNTCNETest() = {
    // note we do not need a class name here, we use the structural type
    def parserNTCNE = new NTCNE(Set('X', 'Y', 'Z'))

    assertTrue(parserNTCNE.parseAll("XYZ".toList)) // true
    assertFalse(parserNTCNE.parseAll("XYYZ".toList)) // false
    assertFalse(parserNTCNE.parseAll("".toList)) // false
  }

  @Test
  def sparserTest() = {
    import ImplicitParser._
    def sparser: Parser[Char] = "abc".charParser()

    assertTrue(sparser.parseAll("aabc".toList)) // true
    assertFalse(sparser.parseAll("aabcdc".toList)) // false
    assertTrue(sparser.parseAll("".toList)) // true
  }
}