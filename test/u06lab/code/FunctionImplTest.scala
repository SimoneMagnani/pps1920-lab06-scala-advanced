package u06lab.code

import org.junit.Assert._
import org.junit.jupiter.api.Test

class FunctionImplTest {
  val f: Functions = FunctionsImpl

  @Test
  def sumTest() = {
    assertEquals(60.1, f.sum(List(10.0, 20.0, 30.1)), 0.01) // 60.1
    assertEquals(0.0, f.sum(List()), 0.01) // 0.0
  }

  @Test
  def concatTest() = {
    assertEquals("abc", f.concat(Seq("a", "b", "c"))) // abc
    assertEquals("", f.concat(Seq())) // ""
  }

  @Test
  def maxTest() = {
    assertEquals(3, f.max(List(-10, 3, -5, 0))) // 3
    assertEquals(-2147483648, f.max(List())) // -2147483648
  }
}
