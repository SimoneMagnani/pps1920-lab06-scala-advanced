package u06lab.code

import org.junit.Assert._
import org.junit.jupiter.api.Test
import u06lab.code.TicTacToe._

class TicTacToeTest {

  @Test
  def findTest(): Unit ={
    assertEquals(Some(X), find(List(Mark(0,0,X)),0,0))
    assertEquals(Some(O), find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),0,1))
    assertEquals(None, find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),1,1))
  }

  @Test
  def placeAnyMarkTest(): Unit ={
    assertEquals(9, placeAnyMark(List(), X).size)
    assertEquals(8, placeAnyMark(List(Mark(0,0,O)), X).size)
  }

  @Test
  def computeAnyGameTest(): Unit ={
    assertEquals(((9*8)*7)*6, computeAnyGame(O, 4).size)
  }

  @Test
  def computeAnyGameWithStopTest(): Unit = {
    assertEquals(((9*8)*7)*6, computeAnyGameWithStop(O, 4).toList.size)
  }

}
